#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>
#include <cassert>
#include "crypto/ellcurve/Ed25519.h"
#include "adnl/utils.hpp"
#include "auto/tl/ton_api.h"
#include "auto/tl/ton_api_json.h"
#include "tl/tl_json.h"
#include "td/utils/OptionsParser.h"
#include "td/utils/filesystem.h"
#include "keys/encryptor.h"
#include "keys/keys.hpp"

int main(int argc, char *argv[]) {

  struct Addr {
    td::IPAddress addr;

    bool operator<(const Addr &with) const {
      return addr < with.addr;
    }
  };

  td::OptionsParser p;
  p.set_description("generate initial keys");

  std::string ipstr = "";

  p.add_option('i', "ip", "your node IP and port", [&](td::Slice key) {
    ipstr = key.str();
    return td::Status::OK();
  });

  auto S = p.run(argc, argv);

  if (S.is_error()) {
    std::cerr << S.move_as_error().message().str() << std::endl;
    return 2;
  }

  auto pk = ton::PrivateKey{ton::privkeys::Ed25519::random()};
  td::write_file("./keys/auto_dht.id", pk.compute_short_id().as_slice()).ensure();
  td::write_file("./keys/auto_dht.pub", pk.compute_public_key().export_as_slice().as_slice()).ensure();
  td::write_file("./keys/auto_dht.pk", pk.export_as_slice()).ensure();

  auto adnl_pk = ton::PrivateKey{ton::privkeys::Ed25519::random()};
  td::write_file("./keys/auto_adnl.id", adnl_pk.compute_short_id().as_slice()).ensure();
  td::write_file("./keys/auto_adnl.pub", adnl_pk.compute_public_key().export_as_slice().as_slice()).ensure();
  td::write_file("./keys/auto_adnl.ed25519.pub", adnl_pk.compute_public_key().ed25519_value().raw().as_slice()).ensure();
  td::write_file("./keys/auto_adnl.pk", adnl_pk.export_as_slice()).ensure();
  
  auto validator_ed25519_pk = ton::privkeys::Ed25519{ton::privkeys::Ed25519::random()};
  auto validator_pk = ton::PrivateKey{validator_ed25519_pk};
  td::write_file("./keys/auto_validator.id", validator_pk.compute_short_id().as_slice()).ensure();
  td::write_file("./keys/auto_validator.pub", validator_pk.compute_public_key().export_as_slice().as_slice()).ensure();
  td::write_file("./keys/auto_validator.ed25519.pub", validator_pk.compute_public_key().ed25519_value().raw().as_slice()).ensure();
  td::write_file("./keys/auto_validator.pk", validator_pk.export_as_slice()).ensure();

  td::write_file("./keys/auto_validator.ed25519.pk", validator_ed25519_pk.export_as_slice()).ensure();

  td::IPAddress ip;
  ip.init_host_port(ipstr);
  auto ipfinal = static_cast<td::int32>(ip.get_ipv4());
  td::write_file("./auto_remote_ip", std::to_string(ipfinal)).ensure();
}
